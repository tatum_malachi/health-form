from flask import request
import json

from . import esquery_app
from generic import ESError, query_es


# Elasticsearch config.py items
es_type_patient = esquery_app.config["ES_TYPE_PATIENT"]


# @esquery_app.route("/api/popular-search-terms/", methods=['GET'])
# def get_popular_search_terms_ext():
# 	query_string = 'size=100000'

# 	try:
# 		results = query_es(es_type_search, query_string = query_string)
  
# 	except ESError:
# 		return "Couldn't download data from the Elasticsearch API.  If this " + \
# 			"error occurs repeatedly, there may be a problem in " + \
# 			"esquery.get_popular_search_terms_ext or generic.query_es.", 400
   
# 	results = results.get('hits').get('hits')
# 	return json.dumps(sorted(results, key=lambda k: k.get('_source').get('totalsearches'), reverse=True))

@esquery_app.route("/api/search/", methods=['GET'])
def search_ES_ext():
	query = request.args.get("query")

	try:  
		results = search_ES(query)
		return json.dumps(results)

	except (AttributeError, TypeError):
		return "You must supply a query.", 400

	except ESError as error:
		return error.message, 400

def search_ES(query):
	query = query.strip()
	data = '{"query": {"bool": {"must": [ {"multi_match": {"query": "' + query + '","fields": "patient_name","fuzziness": "AUTO","operator": "or","prefix_length": 1}}],"should": {"match_phrase": {"_all": "' + query + '" }}}}}'

	try:
		r = query_es(es_type_patient, data=data)
  
	except ESError:
		raise ESError("Couldn't download data from the Elasticsearch API." + \
			"  If this error occurs repeatedly, there may be a problem in " + \
			"esquery.search_ES or generic.query_es.")

	return r

# def exact_search(exact):
# 	exact = exact.strip()
# 	data = '{"query": {"match_phrase": {"all": "' + exact + '" }}}, "size":10000}'

# 	try:
# 		r = query_es(es_type_benefit_details, data=data)
  
# 	except ESError:
# 		raise ESError("Couldn't download data from the Elasticsearch API." + \
# 			"  If this error occurs repeatedly, there may be a problem in " + \
# 			"esquery.exact_search or generic.query_es.")

# 	return r

# def save_search(query):
# 	# ts = time.time()
# 	# st = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
# 	# geo_return = geoip.get_my_ip()

# 	data = '{"size":10000}'
# 	query_string = 'q='+query

# 	try:
# 		r = query_es(es_type_search, data=data, query_string =
# 					  query_string)
  
# 	except ESError:
# 		raise ESError("Couldn't download data from the Elasticsearch API.  " + \
# 			"If this error occurs repeatedly, there may be a problem in " + \
# 			"esquery.save_search or generic.query_es.")

# 	# elasticsearch to check for search term already in type
# 	# if search term exists in type 'search'

# 	# if search term exists in type 'search'
# 	if r.get('hits').get('hits'):
# 		nomatch = True
# 		for row in r.get('hits').get('hits'):
# 			_query = row.get('_source').get('query')
# 			if _query == query: 
# 				nomatch = False
# 				queryid = row.get('_id')
# 				totalsearches = row.get('_source').get('totalsearches')
# 				# update the existing doc
# 				totalsearches +=1
# 				# delete the old doc
# 				try:
# 					r = query_es(es_type_benefit_details, data=data,
# 								  queryid=queryid, method='DELETE')

# 				except ESError:
# 					raise ESError("Couldn't issue 'DELETE' command to the " + \
# 						"Elasticsearch API:  search not saved.  If this ," + \
# 						"error occurs repeatedly there may be a problem in " + \
# 						"esquery.save_search or generic.query_es.")
                          
# 	 	           # upload the new doc
# 				data = '{"query": "'+query+'", "totalsearches":'+str(totalsearches)+'}'

# 				try:
# 					r = query_es(es_type_benefit_details, data=data,
# 								  method='POST')

# 				except ESError:
# 					raise SaveSearchError("Critical error in Elasticsearch " + \
# 						"update:  The old record was deleted, but the new one" + \
# 						" wasn't created.  The update will require a manual " + \
# 						"'POST' operation with data = " + data + ".  If this " + \
# 						"error occurs repeatedly, there may be a problem in " + \
# 						"esquery.save_search or generic.query_es.")

# 		if nomatch:
# 			data = '{"query":"'+query+'", "totalsearches":1}'

# 			try:
# 				r = query_es(es_type_benefit_details, data=data,
# 							  method='POST')
  
# 			except ESError:
# 				raise ESError("Couldn't post data to the Elasticsearch API:" + \
# 					"  search not saved.  If this error occurs repeatedly, " + \
# 					"there may be a problem in esquery.save_search or " + \
# 					"generic.query_es.")

# 	else:
# 		# create and add a new doc
# 		data = '{"query":"'+query+'", "totalsearches":1}'

# 		try:
# 			r = query_es(es_type_benefit_details, data=data, method='POST')
  
# 		except ESError:
# 			raise ESError("Couldn't post data to the Elasticsearch API:  " + \
# 				"search not saved.  If this error occurs repeatedly, there " + \
# 				"may be a problem in esquery.saveSearch or generic.query_es.")


# 	# search['search'] = [{'timestamp': st, 
# 	# 				'user_id':'',
# 	# 				'session_id': session['session_id'],
# 	# 				'city': geo_return.get('city'),
# 	# 				'region_code': geo_return.get('region_code'),
# 	# 				'country_name':geo_return.get('country_name')
# 	# 				 }]
# 	return
