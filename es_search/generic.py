import requests
from time import sleep
from random import randrange

from . import esquery_app


# This exception should be raised when an Elasticsearch query fails.

class ESError(Exception):
    pass


# Run a query through the Elasticsearch API.  In case of an error, re-try five
# times using exponential backoff.

def query_es(query_type, data = None, query_string = None, queryid = None,
             method = "GET"):

    # Elasticsearch config.py items
    es_url = esquery_app.config["ES_URL"]
    es_index = esquery_app.config["ES_INDEX"]
    es_creds = esquery_app.config["ES_CRED"]
    es_user = es_creds["username"]
    es_pwd = es_creds["password"]

    url = es_url + es_index + query_type + '_search'
    if queryid != None:
        url += queryid
    if query_string != None:
        url += "?" + query_string

    # Uncomment these lines to diagnose problems obscured when ESError is
    # raised.
#    if data == None:
#        search_results = requests.request(method='GET', url=url,
#            auth=(es_user, es_pwd), verify=False)
#
#    else:
#           
#        search_results = requests.request(method='GET', url=url,
#            data=data, auth=(es_user, es_pwd), verify=False)

    # Initialize a wait time for exponential backoff.
    wait_time = 1

    # Try the query.
    while True:

        try:

            if data == None:
                search_results = requests.request(method=method, url=url,
                    auth=(es_user, es_pwd), verify=False)

            else:
                
                search_results = requests.request(method=method, url=url,
                    data=data, auth=(es_user, es_pwd), verify=False)

            break

        except:

            if wait_time <= 16:
                print "Error querying the Elasticsearch API.  Re-trying."
                sleep(wait_time + randrange(0, 1000, 1) / 1000)
                wait_time *= 2
                
            else:
                raise ESError()

    search_results = search_results.json()

    return search_results
