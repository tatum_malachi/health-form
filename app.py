#!/usr/bin/env python

from flask import Flask
from flask.ext.bootstrap import Bootstrap

app= Flask(__name__)
app.config.from_object('config.Config')
bootstrap = Bootstrap(app)

import checkinForm

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True, use_reloader=False)