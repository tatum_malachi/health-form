import json
from es_search import *
from es_search.generic import *
import requests

es_type_patient = esquery_app.config["ES_TYPE_PATIENT"]
#print esquery_app.config["ES_URL"]
# repopulate and then change what they want

# get contents of patient in order to autopopulate
def retrieve_patient_details(name):
	es_results = search_ES(name)
	#print es_results
	first_results = es_results['hits']['hits'][0]
	if len(es_results['hits']['hits']) == 1: 
		return first_results
	else:
		return {}
	# patient_id = es_results = first_result['_id']
	# status = first_result['_source']['status']
	# first_name = first_result['_source']['first_name']
	# last_name = first_result['_source']['last_name']
	# address = first_result['_source']['address']
	# phone_number = first_result['_source']['phone_number']
	# doctor_first_name = first_result['_source']['doctor_first_name']
	# doctor_last_name = first_result['_source']['doctor_last_name']
	# insurance_name = first_result['_source']['insurance_name']
	# insurance_group_id = first_result['_source']['insurance_group_id']
	# insurance_member_id = first_result['_source']['insurance_member_id']
	# appointment_time = first_result['_source']['appointment_time']
	#return patient_id, status, first_name, last_name, address, phone_number, doctor_first_name, doctor_last_name
	#return status, first_name, last_name, patient_id

# update request

def update_patient_details(patient_details):
	patient_id = patient_details['patient_id']
	print patient_id
	data = {"doc": patient_details}
	data = json.dumps(data)
	#r = query_es(es_type_patient + "%s/_update" %(str(patient_id)), data=data)
	r = requests.request(method='POST', url=esquery_app.config["ES_URL"] + \
	'%s%s%s/_update' %(esquery_app.config["ES_INDEX"],esquery_app.config["ES_TYPE_PATIENT"],patient_id), data=data,\
	auth=('k44vzqdk','85r625urwcvtkkkd'), verify=False)

	return data

# {"status": "' + s + '", "first_name": "", "last_name": "", "address": "", "phone_number": "", "doctor_first_name": "", "doctor_last_name": ""}