#!/usr/bin/env python

from flask import Flask, render_template, session, redirect, url_for, flash, request, abort, send_file 
from flask.ext.wtf import Form
from wtforms import StringField, SubmitField, SelectField
from wtforms.validators import Required, Optional
from flask.ext.bootstrap import Bootstrap

import time, os

from __main__ import app


#app=Flask(__name__)
#app.config['SECRET_KEY'] = 'my name is not susan! so watch what you say!!'
#app.config.from_object('config.Config')
#bootstrap = Bootstrap(app)

from patient_deets import *
  
#name = 'jeb bush'

#os.system('open http://localhost:5000/')
times = map(lambda x: str(x)+':00', range(0,24))
times = map(lambda x : "%s %s" %(time.strftime("%m/%d/%Y"),x), times)

for count, apptTime in enumerate(times):
    times[count] = (str(count + 1),apptTime)
times.insert(0,('0',''))
    
class NameForm(Form):
    patientStatus = SelectField('Patient Status',choices=[('new','New'),('returning','Returning')])
    patientName = StringField('Patient Name:', validators = [Required()])
    address  = StringField('Address: ', validators = [Optional()])
    phoneNumber = StringField('Phone Number (XXX XXX XXXX): ', validators = [Optional()])
    emailAddress = StringField('Email Address: ', validators = [Required()])
    insuranceName = StringField('Insurance Name:', validators = [Required()])
    insuranceGroupID = StringField('Insurance GroupID:', validators = [Required()])
    insuranceMemberID = StringField('Insurance MemberID:', validators = [Required()])
    doctorName = StringField("Doctor's Name: Dr.", validators = [Optional()])
    apptTime = SelectField('Appointment Time', choices = times, validators = [Optional()])
    
    submit = SubmitField('Submit')

@app.route('/', methods=['GET', 'POST'])
def index():
    
    name = request.args.get('name', '')
    name= name.lower().strip()
    form = NameForm()
    
    try:
        search_results = retrieve_patient_details(name)
    except:
        pass
    
    try: 
        patient_id = search_results['_id']
    except:
        pass
    
    try:
        status = search_results['_source']['status']
        if status == 'new':
            form.patientStatus.data = 'new'
        elif status == 'returning ':
            form.patientStatus.data = 'returning'
        else:
            pass
    except:
        pass
    
    try:
        form.patientName.data = search_results['_source']['patient_name']
    except:
        pass
    
    try:
        form.address.data = search_results['_source']['address']
    except:
        pass

    try:
        form.phoneNumber.data = search_results['_source']['phone_number']
    except:
        pass
    
    try:
        form.doctorName.data = search_results['_source']['doctor_name']
    except:
        pass
    
    try:
        form.insuranceName.data = search_results['_source']['insurance_name']
    except:
        pass
    
    try:
        form.insuranceGroupID.data = search_results['_source']['insurance_group_id']
    except:
        pass
    
    try:
        form.insuranceMemberID.data = search_results['_source']['insurance_member_id']
    except:
        pass
    try:
        form.emailAddress.data = search_results['_source']['email_address']
    except:
        pass

    
    if form.validate_on_submit():
        form = NameForm()
        #print "Name: %s" %(form.patientName.data.strip().lower())
        #print "Patient Status: %s" %(form.patientStatus.data)

        patient_details= {}
        #print type(form.patientStatus.data)
        patient_details['status'] = form.patientStatus.data
        patient_details['patient_name'] = form.patientName.data
        patient_details['patient_id'] = patient_id
        patient_details['email_address'] = form.emailAddress.data
        patient_details['address'] = form.address.data
        patient_details['phone_number'] = form.phoneNumber.data 
        patient_details['doctor_name'] = form.doctorName.data
        patient_details['insurance_name'] = form.insuranceName.data 
        patient_details['insurance_group_id'] = form.insuranceGroupID.data
        patient_details['insurance_member_id'] = form.insuranceMemberID.data
        
        ## need to add appointment time functionality
        #print patient_details
        update_patient_details(patient_details)
        
        return redirect(url_for('submission'))
    
    return render_template('index.html', form=form)

@app.route('/submission')
def submission():
    return render_template('submission.html')


#if __name__ == '__main__':
#    app.run(host='0.0.0.0', debug=True, use_reloader=False)
