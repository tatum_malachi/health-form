import simplejson as json

# default config
class Config(object):

    # Set basic Flask values.
    USE_RELOADER = False
    SECRET_KEY = '6i\xed\xafp.\x111^\xe2\xa3\xe5\xb8zA7\xbf\xf7F\x8e\x96\x90\xa1 '
    
    # Set the Alexa app version and the application ID.
    ALEXA_VERSION = 1.0
    APPLICATION_ID = "amzn1.echo-sdk-ams.app.3ce16ef4-6f31-4999-a844-28099f841086"
    
    # Elasticsearch Parameters
    ES_URL = "https://dogwood-435145.us-east-1.bonsai.io/"
    ES_INDEX = "hamilton/"
    ES_TYPE_PATIENT = "patient/"
    ES_CRED = json.load(open("config/elasticsearch_credentials.json"))
